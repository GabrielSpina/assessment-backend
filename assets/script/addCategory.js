class AddCategory{
    constructor(){
        /*  Esta pagina ao abrir testa se é edição ou adição, se for edição apagar session e qndo enviar a query enviar update
            se for adição envia insert into. */
        if(sessionStorage.getItem("edicao")){
            // Caso seja uma EDIÇÃO
            this.registro = sessionStorage.getItem("edicao");
            sessionStorage.removeItem("edicao");

            this.req= "req=set&tab=categorias";
            console.log("Edição!");

            this.rotinaAdicao();

        } else {
            // Caso seja uma ADIÇÃO
            this.registro;
            this.req = "req=ins&tab=categorias";
            console.log("Adição!");

        }
    }

    rotinaAdicao(){
        let registro = JSON.parse(this.registro);

        let cod = document.getElementById("category-code");
        cod.value = registro.codigo;
        cod.disabled = true;

        document.getElementById("category-name").value = registro.nome;
    }

    preencheReq(){

        let nome = document.getElementById("category-name").value;
        let cod = document.getElementById("category-code").value;

        this.req += `&codigo=${cod}&nome='${nome}'`;
    }

    limpaForm(){
        document.getElementById("category-name").value = "";
        document.getElementById("category-code").value = "";
    }


}

let cat = new AddCategory();

document.addEventListener("submit", (event) => {
    event.preventDefault();
    
    cat.preencheReq();

    let promise = new MyPromise(endAccBanco, cat.req);
    
    promise
        .then((resp) => {
            if(resp == "adicionado") {
                alert("Produto foi "+resp+" com sucesso!");
                cat.limpaForm();
            }else if(resp == "alterado") {
                alert("Produto foi "+resp+" com sucesso!");
                window.open(endCat, "_self"); 
            } else {
                if(resp.slice(0, 9) == "Duplicate"){
                    alert("Ja existe uma categoria cadastrada com este código");
                    console.log(resp);
                } else {
                    alert("Não foi possivel cadastrar este produto\n(consulte log)");
                    console.log(resp);
                }
            }
        })
        .catch((err) => console.log(err))

});