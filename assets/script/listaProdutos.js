class ListaProdutos {
    
    constructor(){
        this.lista = [];
        this.listaCat = [];
        this.listaCatProd = [];
        this.ultRegistro = 0;
        this.table = document.querySelector("table");

        this.carregar10();
        this.addBtn();
    }

    carregar10(){

        let promise = new MyPromise(endAccBanco, "req=sel&tab=produtos&cur="+this.ultRegistro);
        let promiseCat = new MyPromise(endAccBanco, "req=sel&tab=categorias");
        let promiseProdCat = new MyPromise(endAccBanco, "req=sel&tab=relacionamento");

        promiseCat
            .then(resp => {

                this.listaCat = JSON.parse(resp);

                promiseProdCat
                    .then((resp) => {
                        this.listaCatProd  = JSON.parse(resp);
                        
                        promise
                        .then((resp) => {
                            this.ultRegistro += 10;
                            let listaJson = JSON.parse(resp);
                            listaJson.forEach(item => {
                                this.lista.push(item);
                            });
                            this.carregarTabela();
                        })
                        .catch((err) => console.log(err));
                    })
            });
    }

    deletarRegistro(id){
        
        if(confirm("Deseja deletar o produto "+id+"?")){
        let promise = new MyPromise(endAccBanco, "req=del&tab=produtos&sku='"+id+"'");

        promise
            .then((resp) => {
                //Testando qual registro possue a entrada que eu quero apagar
                let registros = document.querySelectorAll("tr");
                registros.forEach((registro) => {
                    if(registro.cells[1].innerHTML == '<span class="data-grid-cell-content">'+id+'</span>'){
                        registro.style.display = "none";
                    }
                });
                alert("item "+id+" apagado com sucesso");
            })
            .catch((err) => console.log(err));
        }
        

    }



    editarProduto(registro){
        //abre uma session e diz q é edição.
        sessionStorage.setItem("edicao", JSON.stringify(registro));
        //redirecionando para parina de adição
        window.open(endAddProd, "_self"); 
    }

    carregarTabela(){
        let ultimos10 = this.lista.slice(this.lista.length-10, this.lista.length) //Pega os ultimos 10 registros carregados

        //Aqui vai iterar por cada registro
        ultimos10.forEach(element => {
            let linha = document.createElement("tr"); // Criando a linha

            // Criando as celulas
            for(var key in element){
                if(key != "descricao" && key != "categoria"){
                    //Elementos do banco
                    let cel = document.createElement("td");
                    cel.setAttribute("class", "data-row");

                    let span = document.createElement("span");
                    span.setAttribute("class", "data-grid-cell-content");

                    let texto = document.createTextNode(element[key]); 

                    span.appendChild(texto);
                    cel.appendChild(span);
                    linha.appendChild(cel); 
                } else if(key == "categoria"){
                    //Testa se existe aquele sku na lista de relacionamento.
                    let exi = false;
                    this.listaCatProd.forEach(item =>{
                        if(item.sku == element.sku) exi = true;
                    });
                    // teste relacionamento.
                    let cel = document.createElement("td");
                    cel.setAttribute("class", "data-row");
                    let span = document.createElement("span");
                    span.setAttribute("class", "data-grid-cell-content");
                    // PROCURAR MELHOR SOLUÇÃO
                    let string = "";
                    if(element.categoria != null && !exi){ //esse teste esta ruim
                        string = element.categoria+" *";
                    } else {
                        this.listaCatProd.forEach(prod => {
                            if(prod.sku == element.sku){
                                this.listaCat.forEach(cat => {
                                    if(prod.codigo == cat.codigo){
                                        string += cat.nome+" ";
                                    }
                                })
                            }
                        })
                    }
                    
                    let texto = document.createTextNode(string); 

                    // PROCURAR MELHOR SOLUÇÃO
                    span.appendChild(texto);
                    cel.appendChild(span);
                    linha.appendChild(cel);
                }
            }

            //btns
            let btns = document.createElement("td");
            btns.setAttribute("class", "data-grid-td");

            let divPai = document.createElement("div");
            divPai.setAttribute("class", "actions");

            let divFirstChild = document.createElement("div");
            divFirstChild.setAttribute("class", "btn-action edit");
            divFirstChild.setAttribute("style", "cursor:pointer");
            let divFCSpan = document.createElement("span");
            let divFCStexto = document.createTextNode("Edit");
            divFCSpan.appendChild(divFCStexto);
            divFirstChild.appendChild(divFCSpan);
            // Colocar aqui o método de editar
            divFirstChild.addEventListener("click", () => {
                this.editarProduto(element);
            });

            let divLastChild = document.createElement("div");
            divLastChild.setAttribute("class", "btn-action delete");
            divLastChild.setAttribute("style", "cursor:pointer");
            let divLCSpan = document.createElement("span");
            let divLCStexto = document.createTextNode("Delete");
            divLCSpan.appendChild(divLCStexto);
            divLastChild.appendChild(divLCSpan);
            // Colocar aqui o método de deletar
            divLastChild.addEventListener("click", () => {
                this.deletarRegistro(element.sku);
            });

            divPai.appendChild(divFirstChild);
            divPai.appendChild(divLastChild);

            linha.appendChild(divPai); 
            
            this.table.appendChild(linha);
        });
    }

    addBtn(){
        let main = document.querySelector("main");
        let btnAdd = document.createElement("button");
        btnAdd.setAttribute("class", "btn-action");
        let texto = document.createTextNode("mais");
        btnAdd.appendChild(texto);
        
        btnAdd.addEventListener("click", () => {
            this.carregar10();            
        });

        main.appendChild(btnAdd);
    }
}

lista = new ListaProdutos();

