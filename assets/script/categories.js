class Categorias {

    constructor(){

        this.lista = [];
        this.table = document.querySelector("table");

        this.carregarLista();
    }

    carregarLista(){
        
        let promise = new MyPromise(endAccBanco, "req=sel&tab=categorias");

        promise 
            .then((res) => {
                this.lista = JSON.parse(res);
                this.carregarTabela();
            })
            .catch((err) => console.log(err))
    }

    carregarTabela(){

        //Se der tempo abstrair esta função
        //let carregador = new CarregaTabela(this.table, this.lista, this.deletar, this.editar);

        this.lista.forEach((element) => { //lista json retornada do banco <---
            let linha = document.createElement("tr");

            //Colocando os dados
            for(let key in element){

                let cel = document.createElement("td");
                cel.setAttribute("class", "data-row");
                
                let span = document.createElement("span");
                span.setAttribute("class", "data-grid-cell-content");
                
                let texto = document.createTextNode(element[key]); 
                
                span.appendChild(texto);
                cel.appendChild(span);
                linha.appendChild(cel); 
            }

            //btns
            let btns = document.createElement("td");
            btns.setAttribute("class", "data-grid-td");

            let divPai = document.createElement("div");
            divPai.setAttribute("class", "actions");

            let divFirstChild = document.createElement("div");
            divFirstChild.setAttribute("class", "btn-action edit");
            divFirstChild.setAttribute("style", "cursor:pointer");
            let divFCSpan = document.createElement("span");
            let divFCStexto = document.createTextNode("Edit");
            divFCSpan.appendChild(divFCStexto);
            divFirstChild.appendChild(divFCSpan);
            // Colocar aqui o método de editar
            divFirstChild.addEventListener("click", () => this.editar(element));

            let divLastChild = document.createElement("div");
            divLastChild.setAttribute("class", "btn-action delete");
            divLastChild.setAttribute("style", "cursor:pointer");
            let divLCSpan = document.createElement("span");
            let divLCStexto = document.createTextNode("Delete");
            divLCSpan.appendChild(divLCStexto);
            divLastChild.appendChild(divLCSpan);
            // Colocar aqui o método de deletar
            divLastChild.addEventListener("click", () => this.deletar(element));

            divPai.appendChild(divFirstChild);
            divPai.appendChild(divLastChild);

            linha.appendChild(divPai); 

            this.table.appendChild(linha);


        });
    }

    deletar(element){
        if(confirm("Tem certeza que deseja deletar a categoria "+element.nome+"?")){
            let promise = new MyPromise(endAccBanco, "req=del&tab=categorias&codigo="+element.codigo);

            promise
                .then((resp) => {
                    //Testando qual registro possue a entrada que eu quero apagar
                    let registros = document.querySelectorAll("tr");
                    registros.forEach((registro) => {
                        if(registro.cells[1].innerHTML == '<span class="data-grid-cell-content">'+element.codigo+'</span>'){
                            registro.style.display = "none";
                        }
                    });
                    alert("A categoria "+element.nome+" foi apagada com sucesso");
                })
                .catch((err) => console.log(err))
        }
    }

    editar(registro){
        //abre uma session e diz q é edição.
        sessionStorage.setItem("edicao", JSON.stringify(registro));
        //redirecionando para parina de adição
        window.open(endAddCat, "_self"); 
    }

}

let cat = new Categorias();