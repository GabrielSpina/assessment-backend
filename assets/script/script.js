const endAccBanco = "http://localhost/desafio/back/acessaBanco.php";
const endAddProd = "http://localhost/desafio/addProduct.html";
const endProd = "http://localhost/desafio/products.html";
const endAddCat = "http://localhost/desafio/addCategory.html";
const endCat = "http://localhost/desafio/categories.html"


class MyPromise{
    //Recebe o endereço e uma ação e retorna uma promise.
    constructor(end, acao){

        return new Promise(
            (resolve, reject) => {
                let http = new XMLHttpRequest();
        
                http.onreadystatechange = function(){
                    if (this.readyState == 4 && this.status == 200){
                        resolve(this.responseText);
                    } else if (this.readyState == 4 && this.status != 200) {
                        reject(this.responseText);  
                    } 
                }
        
                
                http.open("POST", end);
                http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                http.send(acao);
            }
        );
    }
}