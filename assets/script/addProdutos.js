
class AddProdutos {
    constructor(){
        /*  Esta pagina ao abrir testa se é edição ou adição, se for edição apagar session e qndo enviar a query enviar update
            se for adição envia insert into. */
        if(sessionStorage.getItem("edicao")){
            // Caso seja uma EDIÇÃO
            this.registro = sessionStorage.getItem("edicao");
            sessionStorage.removeItem("edicao");

            this.req= "req=set&tab=produtos";
            console.log("Edição!");

            this.rotinaAdicao();

        } else {
            // Caso seja uma ADIÇÃO
            this.registro;
            this.req = "req=ins&tab=produtos";
            console.log("Adição!");

        }

        this.carregaCategorias();

        //variavel só p/ guardar as categorias
        this.categoriasSel = [];
    }

    rotinaAdicao(){
        // Quando é edição e os campos precisam ser completados. (falta categorias)
        let registro = JSON.parse(this.registro);
        console.log(registro);
        let sku = document.getElementById("sku");
        sku.value = registro.sku;
        sku.disabled = true;

        document.getElementById("name").value = registro.nome;
        document.getElementById("price").value = registro.preco;
        document.getElementById("quantity").value = registro.quantidade;
        document.getElementById("category").value = registro.categoria; // ALTERAR PARA RECEBER VARIAS
        document.getElementById("description").value = registro.descricao;
    }

    preencheReq(){

        let sku = document.getElementById("sku").value;
        let name = document.getElementById("name").value;
        let price = document.getElementById("price").value;
        let quantity = document.getElementById("quantity").value;
        let category = this.categoriasSel;
        let description = document.getElementById("description").value;  

        this.req += `&sku='${sku}'&categoria=${category}&nome='${name}'&descricao='${description}'&quantidade=${quantity}&preco=${price}`;
    }

    limpaForm(){
        document.getElementById("sku").value = "";
        document.getElementById("name").value = "";
        document.getElementById("price").value = "";
        document.getElementById("quantity").value = "";
        document.getElementById("category").value = "";
        document.getElementById("description").value = "";
    }

    carregaCategorias(){
        let promise = new MyPromise(endAccBanco, "req=sel&tab=categorias");
        promise
            .then((resp) => {
                let categorias = JSON.parse(resp);
                let listaCat = document.getElementById("category");
                categorias.forEach(element => {
                    let opt = document.createElement("option");
                    opt.setAttribute("value", element.codigo);
                    let texto = document.createTextNode(element.nome);
                    opt.appendChild(texto);

                    opt.addEventListener("click", () => {
                        if(opt.style.color == "red"){
                            //Tira da lista
                            opt.style.color = "black";
                            this.categoriasSel.splice(this.categoriasSel.indexOf(element.codigo),1);

                        } else {
                            //Coloca na lista
                            opt.style.color = "red";
                            this.categoriasSel.push(element.codigo);
                            
                        }
                    })
                    listaCat.appendChild(opt);
                });

            })
            .catch((resp) => console.log(resp))
    }

}

let addprod = new AddProdutos();

//Carrega a lista de categorias


document.addEventListener("submit", (event) => {
    // Quando for dado o submit entra nesta rotina
    event.preventDefault();
    
    addprod.preencheReq();

    let promise = new MyPromise(endAccBanco, addprod.req);
    
    promise
        .then((resp) => {

            
            if(resp == "adicionado") {
                alert("Produto foi "+resp+" com sucesso!");
                addprod.limpaForm();
            }else if(resp == "alterado") {
                alert("Produto foi "+resp+" com sucesso!");
                window.open(endProd, "_self"); 
            } else {
                if(resp.slice(0, 9) == "Duplicate"){
                    alert("Ja existe um produto cadastrado com este SKU ");
                    console.log(resp);
                } else {
                    alert("Não foi possivel cadastrar este produto\n(consulte log)");
                    console.log(resp);
                }

            }
            
        })
        .catch((err) => console.log(err))

});