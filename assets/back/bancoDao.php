<?php

class Banco {  
    // Dados do banco de dados
    public $end = "127.0.0.1";
    public $user = "root";
    public $senha = "";
    public $base = "assessment";

    public function accBancoRes($conexao, $query){
        /*
            Faz acessos ao banco que retornam um
            json com os registros
        */
        if($res = mysqli_query($conexao, $query)){
            $registros = [];
            while($registro = mysqli_fetch_assoc($res)){
                array_push($registros, $registro);
            }
            return json_encode($registros);
        } else {
            return mysqli_error($conexao);
        }
    }

    public function accBanco($conexao, $query){
        /*
            Faz acessos ao banco que retornam um
            json com os registros
        */

        if(mysqli_query($conexao, $query)){
            echo false;
        } else {
            echo mysqli_error($conexao);    
        }
    }


}

?>