<?php
require_once("bancoDao.php");

$banco = new Banco();
$conexao = mysqli_connect($banco->end, $banco->user, $banco->senha, $banco->base);

if(array_key_exists("req", $_POST) && $_POST["req"] == "sel"){
    /*  
        *** SELECT ***

        Quando o parametro "req" for "sel" este bloco testa se deve fazer um SELECT 
        com 10 registros ou com todos. 
        Para isso precisamos dos argumentos:
        "tabela"        tabela que sera feito o select
        "cur"           caso queira apenas 10 registros
    */

    $tabela = $_POST["tab"];

    if(array_key_exists("cur", $_POST)){
        $cur = $_POST["cur"];
        $query = "SELECT * FROM $tabela LIMIT 10 OFFSET ".$_POST["cur"];
    } else {
        $query = "SELECT * FROM $tabela;";
    }

    echo $banco->accBancoRes($conexao, $query);
}

if(array_key_exists("req", $_POST) && $_POST["req"] == "selesp"){
    // SOLUÇÃO INEFICAZ ATÉ O MOMENTO
    $query = "SELECT * FROM produtos as p LEFT JOIN relacionamento as r ON p.sku = r.sku";
    echo $banco->accBancoRes($conexao, $query);
}


if(array_key_exists("req", $_POST) && $_POST["req"] == "del"){
    /*
        *** DELETE ***

        Função que recebe um:
        "tab"       tabela que sera feito o select
        O terceiro elemento deve ter como:
            chave       nome da coluna da chave primaria 
            valor       o valor da chave
    */
    $tabela = $_POST["tab"];
    
    for($i=0; $i<=1; $i++) next($_POST); // para caminhar até o 2
    $pk = key($_POST);
    $pkVal = current($_POST);

    $query = "DELETE FROM $tabela WHERE $pk = $pkVal;";

    echo $banco->accBanco($conexao, $query);
    registroLog($query);
}

if(array_key_exists("req", $_POST) && $_POST["req"] == "ins"){
    /*
        *** INSERT ***

        Função deve receber:
        "tabela"        tabela que sera feito o select
        Uma lista dos das colunas a serem inclusas no registro;
     */
    $tabela = $_POST["tab"];

    if($tabela == "produtos") $categoria = $_POST["categoria"]; //Categorias faram um select especial.

    $chaves = "";
    $valores = "";
    for($i=0; $i < count($_POST) ; $i++){ // Pegando as chaves e o valores
        if($i >= 2 && key($_POST) != "categoria"){
            if($i == 2) {
                // if p/ fazer a inserção de categorias
                $pk = key($_POST);
                $pkVal = current($_POST);
            } 
            $chaves .= key($_POST);
            $valores .= current($_POST);
            if($i < count($_POST) -1) {
                $chaves .= ", ";
                $valores .= ", ";
            }
        }
        next($_POST);
    }

    $query = "INSERT INTO $tabela ($chaves) VALUES($valores);";
    if(!$banco->accBanco($conexao, $query)){
        registroLog($query);
        echo "adicionado";
        if($tabela == "produtos") insertCategorias($categoria, $pkVal, $banco, $conexao);
    } else {
        echo mysqli_error($conexao); 
    }
    
}

function insertCategorias($categoria, $pkVal, $banco, $conexao){
    $cats = explode(",", $categoria);
    $catsString = "";
    for($i=0; $i<count($cats); $i++){
        $catsString .= "(".$pkVal.", ".$cats[$i].")";
        if($i < count($cats)-1) $catsString .= ", ";
    }
    $query = " INSERT INTO relacionamento (sku, codigo) VALUES".$catsString.";";
    $banco->accBanco($conexao, $query);
    registroLog($query);
}

if(array_key_exists("req", $_POST) && $_POST["req"] == "set"){
    /*
        *** UPDATE ***

        Função recebe:
        "id"        que indicara o registro a ser alterado
        Lista com o valor das colunas do registro que sera alterado
    */
    $tabela = $_POST["tab"];

    if($tabela == "produtos") $categoria = $_POST["categoria"]; //Categorias faram um select especial.
    

    $valores = "";
    for($i=0; $i < count($_POST) ; $i++){ 
        if($i >= 3 && key($_POST) != "categoria"){
            $valores .= key($_POST)."=".current($_POST);
            if($i < count($_POST)-1) $valores .= ", ";
            
        } else if ($i == 2) {
            $pk = key($_POST);
            $pkVal = current($_POST);
        }
        next($_POST);
    }
    
    $query = "UPDATE $tabela SET $valores WHERE $pk = $pkVal;";
    registroLog($query);
    if(!$banco->accBanco($conexao, $query)){
        echo "alterado";
        if($tabela == "produtos") alteraCategoria($categoria, $pkVal, $banco, $conexao);
    } else {
        echo mysqli_error($conexao); 
    }
    
}

function alteraCategoria($categoria, $pkVal, $banco, $conexao){
    $query = "DELETE FROM relacionamento WHERE sku = $pkVal;";
    $banco->accBanco($conexao, $query);
    insertCategorias($categoria, $pkVal, $banco, $conexao);
}

mysqli_close($conexao);

function registroLog($msg){
    global $_SESSION;

    $usr = (isset($_SESSION["usr"])) ? $_SESSION["usr"] : "anonimo";

    $arqLog = fopen("registro.txt", "a+");
    
    fwrite($arqLog, "Usuario: ".$usr."\n".date("Y/m/d")." ".date("h:i:sa")."\nExecutou a query: ".$msg."\n\n");
    
    fclose($arqLog );
}

die;