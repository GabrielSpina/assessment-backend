#!/urs/bin/php

<?php
/*  # Codigo desenvolvido para rodar localmente e por isso não usa das classes dos arquivos destinados para o servidor
    # Como foi desenvolvido apenas um script as alterações só poderão ser feitas aqui, se tivesemos mais um txt, poderiamos
    incluir tags nos argumentos para mudar configurações como endereço de servidor etc.
    # Como "sku" foi o codigo de identificação do registro este script o tornou unico, regitando alguns registros.
*/
//VARIAVEIS QUE PODERIAM SER CONFIGURADAS COM AUXILIO DE UM .TXT

$extVal = "csv";

class Banco {  
    // Dados do banco de dados
    public $end = "127.0.0.1";
    public $user = "root";
    public $senha = "";
    public $base = "assessment";
}

//VARIAVEIS DA APP

$arq = isset($argv[1]) ? $argv[1] : false; // Nome do arquivo passado pelo terminal.

//FUNÇÕES

function testaArquivo($arq, $extVal){
    // Testa se arquivo existe e se a extensão esta correta.
    if(!file_exists($arq)){
        // Testa se existe o arquivo.
        echo "Arquivo $arq não encontrado\n";
        die;
    } else {
        // Testa se o arquivo é da extensão desejada.
        $ext = pathinfo($arq, PATHINFO_EXTENSION);
        if($ext != $extVal){
            echo "O arquivo $arq não possue uma extesão valida\n";
            die;
        } else {
            return true;
        }
    }
}

function inclueItem($tabela, $lista){
    /* Função que indicamos a tabela e a lista de itens a incluir 
       e estes itens (ou item) são cadastrados no banco configurado 
       e cria log de erros caso algum ocorra */

    $success = 0;
    $fail = 0;
    $log = [];

    $banco = new Banco();
    $conexao = mysqli_connect($banco->end, $banco->user, $banco->senha, $banco->base);
    
    //Nesta parte o sistema vai tentar incluir todos os itens de uma vez só, aumentando a performance 
    $queryAll = criaQuery($tabela, $lista);
    if(mysqli_query($conexao, $queryAll)){
        echo "Todos os registros foram adicionados com sucesso!\n";
    } else { 
        // Caso o sistema não consiga adicionar o registros de uma unica vez, tenta adicionalos separadamente.
        echo "Um ou mais registros não puderam ser adicionados!\nO sistema esta tentando adicionar os registros separadamente!\nO processo pode demorar alguns minutos e vai gerar um relatorio de erros chama 'log_erros_import.txt'";
        
        foreach($lista as $item){
            $query = "INSERT INTO `$tabela` (nome, sku, descricao, quantidade, preco, categoria) 
                      VALUES(".implode(", ", $item).");"; 
        
            if(!mysqli_query($conexao, $query)){
                $fail++;
                $erro = [$item, mysqli_error($conexao)];
                array_push($log, $erro);
            } else { $success++; }
        }

        echo "$success itens adicionados com sucesso\n$fail itens falharam\n";
    }
    
    // Cria arquivo de log de erros
    if($fail){
        $arqLog = fopen("log_erros_import.txt", "a+");
        foreach($log as $erro){
            fwrite($arqLog, "Erro: ".$erro[1]."\nNo registro: ".implode("\t", $erro[0])."\n\n");
        }
        fclose($arqLog );
    }
    
    mysqli_close($conexao);
}

function pegaArquivo($arq){
    /* Função que pega os registros do arquivo apontado e coloca em uma lista
       que a função "inclueItem()" consegue usar */
    $listaRegistros = [];
    
       $handle = fopen($arq, "r");

    while(($data = fgetcsv($handle, 1000, ";")) != false){
        $data[0] = "'".$data[0]."'"; //Pensar outra solução*** 
        $data[1] = "'".$data[1]."'";
        $data[2] = "'".$data[2]."'";
        $data[5] = "'".$data[5]."'";

        $data[3] = intval($data[3]);
        $data[4] = floatval($data[4]);

        array_push($listaRegistros, $data);
    }
    array_shift($listaRegistros); //Tira o primeiro reistro que são as listas de categorias (mas poderiamos usa-las p/ conferir a tabela)

    fclose($handle);

    return $listaRegistros;
}

function criaQuery($tabela, $lista){
    //Função que pega a lista de registros transforma em uma string com todas as querys e envia de uma só vez.
    $query = "INSERT INTO `$tabela` (nome, sku, descricao, quantidade, preco, categoria) VALUES";
    for($i = 0 ; $i < sizeof($lista); $i++){
        $query = $query."(".implode(", ", $lista[$i]).")"; 
        if($i < sizeof($lista)-1){
            $query = $query.",\n";
        } else {
            $query = $query.";";
        }
    }
    return $query;
}

//MAIN

if($arq && testaArquivo($arq, $extVal)){
    $lista = pegaArquivo($arq);
    inclueItem("produtos", $lista);
    //$query = criaQuery("produtos", $lista);
    //echo $query;
}

die;